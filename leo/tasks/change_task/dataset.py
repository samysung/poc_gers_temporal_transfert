# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
#
import os
import random
from logging import getLogger
from typing import Callable, Dict, Tuple, List, Optional
from torch.utils.data import Dataset
import geopandas as gpd
import rasterio as rio
from rasterio.plot import reshape_as_image
from leo.core.raster import get_img
from albumentations.pytorch import ToTensorV2
from dataclasses import dataclass
from leo.core.image import img_to_tensor
from leo import LOGGER
from leo.tasks.change_task.conf import ChangeTaskConf, TrainConf, ValConf, TestConf
from leo.tasks.change_task.transform import ChangeTaskDataAugmentation, ChangeTaskTestDataAugmentation
from leo.core.default_conf import EnvConf


class ChangeTaskDataset(Dataset):

    def __init__(
                 self,
                 db: gpd.geodataframe,
                 transform: ChangeTaskDataAugmentation,
                 seg_conf: ChangeTaskConf
                ):

        self.seg_conf: ChangeTaskConf = seg_conf
        self.db: gpd.GeoDataFrame = db
        self.length: int = len(db)
        self.transform = transform
        # self.cols: Dict[str, str]
        self.debug: bool = self.seg_conf.debug

    def __len__(self):

        return self.length

    def __getitem__(self, index):

        row = self.db.iloc[index]
        out = dict()
        out["index"] = index
        gamma = 1
        gamma_2 = 1

        if self.seg_conf.multi_style_training:  # use randomly 2019 and 2016 histogram transfer style
            gamma: float = random.uniform(0, 1)
            gamma_2: Optional[float] = random.uniform(0, 1) if self.seg_conf.multi_temporal is True else 1
            # LOGGER.info(gamma)

        img_bands = self.seg_conf.img_bands if gamma > 0.5 else self.seg_conf.img_other_style_bands
        img = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.img_field]), indexes=img_bands)
        mask = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.mask_field]),
                       indexes=self.seg_conf.mask_bands)
        change_mask = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.test_conf.mask_change_field]),
                       indexes=self.seg_conf.test_conf.mask_change_bands)

        if self.seg_conf.multi_temporal:

            img_bands = self.seg_conf.img_bands if gamma_2 > 0.5 else self.seg_conf.img_other_style_bands
            img_2 = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.img_2_field]), indexes=img_bands)
            mask_2 = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.mask_2_field]),
                             indexes=self.seg_conf.mask_bands)
            if self.transform is not None:

                transformed = self.transform(img=img, mask=mask, change_mask=change_mask, img_2=img_2, mask_2=mask_2)
                out["img"], out["mask"] = transformed["image"].float(), transformed["mask"].long()
                out["change_mask"] = transformed["change_mask"].long()
                out["img_2"], out["mask_2"] = transformed["image_2"].float(), transformed["mask_2"].long()

            else:

                out["img_2"] = img_to_tensor(img_2)
                out["mask_2"] = img_to_tensor(mask_2)
                out["img"] = img_to_tensor(img)
                out["mask"] = img_to_tensor(mask)
                out["change_mask"] = img_to_tensor(change_mask)

        else:
            if self.transform is not None:

                transformed = self.transform(img, mask, change_mask=change_mask)
                out["img"], out["mask"] = transformed["image"].float(), transformed["mask"].long()
                out["change_mask"] = transformed["change_mask"].long()
            else:
                out["img"] = img_to_tensor(img)
                out["mask"] = img_to_tensor(mask)
                out["change_mask"] = img_to_tensor(change_mask)

        if self.debug:

            out["ori_img"] = img_to_tensor(img)
            out["ori_mask"] = img_to_tensor(mask)

        return out


class ChangeTaskTestDataset(ChangeTaskDataset):

    def __init__(
                 self,
                 db: gpd.geodataframe,
                 transform: ChangeTaskTestDataAugmentation,
                 seg_conf: ChangeTaskConf
                ):

        super(ChangeTaskTestDataset, self).__init__(db, transform, seg_conf)

    def __len__(self):

        return self.length

    def __getitem__(self, index):
        """
        During Test, this dataset will return
        :param index:
        :return:
        """

        row = self.db.iloc[index]

        out = dict()
        out["index"] = index
        out["updated"] = int(row[self.seg_conf.is_updated_col])

        img_2019 = get_img(os.path.join(self.seg_conf.data_dir,
                                        row[self.seg_conf.test_conf.img_2019_field]),
                                        indexes=self.seg_conf.test_conf.img_2019_bands)
        img_2016_style_2019 = get_img(os.path.join(self.seg_conf.data_dir,
                                        row[self.seg_conf.test_conf.img_2016_style_2019_field]),
                                        indexes=self.seg_conf.test_conf.img_2016_style_2019_bands)
        img_2016_style_2016 = get_img(os.path.join(self.seg_conf.data_dir,
                                        row[self.seg_conf.test_conf.img_2016_style_2016_field]),
                                        indexes=self.seg_conf.test_conf.img_2016_style_2016_bands)
        mask_2019 = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.test_conf.mask_2019_field]),
                                        indexes=self.seg_conf.test_conf.mask_2019_bands)

        if row[self.seg_conf.is_updated_col] == 1:

            mask_2016 = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.test_conf.mask_2016_field]),
                                indexes=self.seg_conf.test_conf.mask_2016_bands)
            change_mask = get_img(os.path.join(self.seg_conf.data_dir, row[self.seg_conf.test_conf.mask_change_field]),
                                indexes=self.seg_conf.test_conf.mask_change_bands)

        else:

            mask_2016, change_mask = mask_2019, mask_2019

        if self.transform is not None:

            transformed = self.transform(img=img_2019,
                                         mask=mask_2019,
                                         img_2016_style_2016=img_2016_style_2016,
                                         img_2016_style_2019=img_2016_style_2019,
                                         mask_2016=mask_2016,
                                         change_mask=change_mask)

            out["img"], out["mask"] = transformed["image"].float(), transformed["mask"].long()
            out["img_2016_style_2016"] = transformed["img_2016_style_2016"].float()
            out["img_2016_style_2019"] = transformed["img_2016_style_2019"].float()
            out["mask_2016"] = transformed["mask_2016"].long()
            out["change_mask"] = transformed["change_mask"].long()

        else:

            out["img_2019"] = img_to_tensor(img_2019)
            out["mask_2019"] = img_to_tensor(mask_2019)
            out["img_2016_style_2016"] = img_to_tensor(img_2016_style_2016)
            out["mask_2016"] = img_to_tensor(mask_2016)
            out["img_2016_style_2019"] = img_to_tensor(img_2016_style_2019)
            out["change_mask"] = img_to_tensor(change_mask)

        return out


if __name__ == "__main__":

    """
    gdf = gpd.read_file(sys.argv[1]).sample(10)
    path_to_img = sys.argv[2]
    cols = ["path_to_irc_1", "path_to_irc_2"]
    dataset = SupervisedTupleImgDataset(gdf, cols, path_to_img, img_backend="pil", trans_backend="albu")
    dataloader = DataLoader(dataset, shuffle=False, batch_size=1)
    fig = plt.figure(figsize=(20, 20))
    i=0

    for sample in tqdm(dataloader, total=len(dataloader)):

        print(len(sample))
        idx, label, img_1, img_2 = sample
        img_1, img_2 = torch.squeeze(img_1), torch.squeeze(img_2)

        if i >= 5:
            exit(0)

        img_1, img_2 = (img_1.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8),\
                       (img_2.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8)

        label = label.numpy()
        print(f"shape of img 1 after: {img_1.shape}")
        print(f"shape of img 2 after: {img_1.shape}")
        print(f"label of img: {label}")
        fig.suptitle(f'exemple 33 image {i}', fontsize=16)
        ax = []
        ax.append(fig.add_subplot(2, 2, 1))
        ax[-1].clear()
        ax[-1].set_title("image 1")
        plt.imshow(img_1)
        # axarr[0, 0].set_title("img 1")
        ax.append(fig.add_subplot(2, 2, 2))
        ax[-1].clear()
        ax[-1].set_title("image 2")
        plt.imshow(img_2)
        # axarr[0, 1].set_title("img 2")
        # axarr[1, 1].set_title("sub img 2")
        plt.pause(1)
        i += 1

    plt.show()
    """
    pass
