import os
from typing import Optional
from torch.utils.data import DataLoader, Dataset, WeightedRandomSampler
from pytorch_lightning import LightningDataModule
import geopandas as gpd
from leo.tasks.change_task.dataset import ChangeTaskDataset, ChangeTaskTestDataset
from leo.tasks.change_task.transform import ChangeTaskDataAugmentation, ChangeTaskTestDataAugmentation
from leo.tasks.change_task.conf import ChangeTaskConf
import albumentations as A
from leo import LOGGER


class ChangeTaskDataModule(LightningDataModule):

    def __init__(self,
                 seg_conf: ChangeTaskConf
                 ):

        super().__init__()
        self.seg_conf: ChangeTaskConf = seg_conf
        self.data_dir = os.path.join(self.seg_conf.env_conf.root, self.seg_conf.data_path)
        self.db_path: str = os.path.join(self.data_dir, self.seg_conf.db_name)
        self.db: gpd.GeoDataFrame = gpd.read_file(self.db_path)
        self.train_fold = f'fold_{str(self.seg_conf.fold)}_train'
        self.val_fold = f'fold_{str(self.seg_conf.fold)}_val'
        self.db_train: gpd.GeoDataFrame = self.db[self.db[self.train_fold] == 1].sample(50)
        # self.db_train = self.db_train.sample(len(self.db_train)) # shuffle
        self.db_val: gpd.GeoDataFrame = self.db[self.db[self.val_fold] == 1].sample(20)
        self.db_val = self.db_val.sample(len(self.db_val))
        self.db_test: gpd.GeoDataFrame = self.db[(self.db["test"] == 1) & (self.db[seg_conf.is_updated_col] == 1)].sample(20)
        self.db_test.reset_index(inplace=True)

        LOGGER.info(self.db_test["id"].unique())
        LOGGER.info(len(self.db_test))
        LOGGER.info(self.db_test)
        self.train: Dataset
        self.val: Dataset
        self.test: Dataset
        if self.seg_conf.weighted_random_sampler:
            self.weights = self.db_train[self.seg_conf.weighted_random_sampler_col].values

        SoftSemanticSegmentationPipe = [
                                        A.OneOf([A.RandomResizedCrop(height=seg_conf.output_size,
                                                                     width=seg_conf.output_size,
                                                                     scale=(0.5, 1.5), p=1.0),
                                                 A.RandomCrop(height=seg_conf.output_size,
                                                              width=seg_conf.output_size)], p=1.0),
                                        A.RandomRotate90(p=0.5),
                                        A.OneOf([A.HorizontalFlip(p=0.5), A.VerticalFlip(p=0.5)], p=0.75),
                                        A.Transpose(p=0.5)
        ]
        HardSemanticSegmentationPipe = [A.OneOf([A.RandomResizedCrop(height=seg_conf.output_size,
                                                                     width=seg_conf.output_size,
                                                                     scale=(0.5, 1.5), p=1.0),
                                                 A.RandomCrop(height=seg_conf.output_size, width=seg_conf.output_size)],
                                                 p=1.0),
                                        A.RandomRotate90(p=0.5),
                                        A.OneOf([A.HorizontalFlip(p=0.5), A.VerticalFlip(p=0.5)], p=0.75),
                                        A.Transpose(p=0.5),
                                        A.GaussianBlur(p=0.5, blur_limit=(3, 3)),
                                        A.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.2, p=0.5)]

        self.train_aug_pipe: list = SoftSemanticSegmentationPipe if self.seg_conf.mode == "soft_aug" else HardSemanticSegmentationPipe
        # self.train_aug_pipe: list = []
        self.val_aug_pipe: list = []
        self.test_aug_pipe: list = []

    def prepare_data(self, db_path):
        self.db = gpd.read_file(db_path)

    def setup(self, stage: Optional[str] = None):

        transform_train = ChangeTaskDataAugmentation(mean=self.seg_conf.mean,
                                                     std=self.seg_conf.std,
                                                     pipe=self.train_aug_pipe,
                                                     multi_temporal=self.seg_conf.multi_temporal)
        transform_val = ChangeTaskDataAugmentation(mean=self.seg_conf.mean,
                                                   std=self.seg_conf.std,
                                                   pipe=self.val_aug_pipe,
                                                   multi_temporal=self.seg_conf.multi_temporal)
        self.train = ChangeTaskDataset(self.db_train, transform=transform_train, seg_conf=self.seg_conf)
        self.val = ChangeTaskDataset(self.db_val, transform=transform_val, seg_conf=self.seg_conf)
        transform_test = ChangeTaskTestDataAugmentation(mean=self.seg_conf.mean, std=self.seg_conf.std,
                                                              pipe=self.test_aug_pipe)
        self.test = ChangeTaskTestDataset(self.db_test, transform=transform_test, seg_conf=self.seg_conf)

    def train_dataloader(self):

        if self.weights is not None:
            return DataLoader(self.train, batch_size=self.seg_conf.train_conf.batch_size,
                              pin_memory=self.seg_conf.train_conf.pin_memory,
                              drop_last=self.seg_conf.train_conf.drop_last,
                              num_workers=self.seg_conf.train_conf.num_workers,
                              sampler=WeightedRandomSampler(weights=self.weights,
                                                            num_samples=len(self.db_train),
                                                            replacement=True))
        else:
            return DataLoader(self.train, batch_size=self.seg_conf.train_conf.batch_size,
                              pin_memory=self.seg_conf.train_conf.pin_memory,
                              shuffle=self.seg_conf.train_conf.shuffle,
                              drop_last=self.seg_conf.train_conf.drop_last,
                              num_workers=self.seg_conf.train_conf.num_workers)

    def val_dataloader(self):

        return DataLoader(self.val,
                          batch_size=self.seg_conf.val_conf.batch_size,
                          pin_memory=self.seg_conf.val_conf.pin_memory,
                          shuffle=self.seg_conf.val_conf.shuffle,
                          drop_last=self.seg_conf.val_conf.drop_last,
                          num_workers=self.seg_conf.val_conf.num_workers)

    def test_dataloader(self):
        return DataLoader(self.test,
                          batch_size=self.seg_conf.test_conf.batch_size,
                          pin_memory=self.seg_conf.test_conf.pin_memory,
                          shuffle=self.seg_conf.test_conf.shuffle,
                          drop_last=self.seg_conf.test_conf.drop_last,
                          num_workers=self.seg_conf.test_conf.num_workers)

    def unsupervised_train_dataloader(self):
        return DataLoader(self.train,
                          batch_size=self.seg_conf.train_conf.batch_size,
                          pin_memory=self.seg_conf.train_conf.pin_memory,
                          shuffle=self.seg_conf.train_conf.shuffle,
                          drop_last=self.seg_conf.train_conf.drop_last)
