import os
from pathlib import Path
import matplotlib.pyplot as plt
# import comet_ml
import torch
import torchvision
from kornia import tensor_to_image
import datetime
from dataclasses import asdict
from leo.tasks.segmentation_task.transform import DeNormalize
from leo.core.default_conf import EnvConf
from leo.core.constants import IMAGENET
from leo.core.image import tensor_to_image
from leo.tasks.change_task.conf import ChangeTaskConf, TrainConf, ValConf, TestConf
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from pytorch_lightning import loggers as pl_loggers
from leo.tasks.change_task.data_module import ChangeTaskDataModule
from leo.tasks.change_task.model import ChangeTaskModel
from leo.tasks.change_task.callback import LogPredictionsCallback, MetricCallBack
from leo.core.io_utils import save_dict_as_json


def main():

    checkpoint = "/var/data/dl/gers/log/test_weighted_sampler_and_rescaled_crop_semantic_segmentation_run-21-11-29-17-35-50/checkpoint/epoch-epoch=434-loss-mean_val_iou=0.75.ckpt"
    seg_conf = ChangeTaskConf()
    print(seg_conf)
    save_dict_as_json(asdict(seg_conf), os.path.join(seg_conf.path_model_output, "hparams.json"))

    data_module = ChangeTaskDataModule(seg_conf=seg_conf)
    data_module.setup(stage="test")
    gdf = data_module.db_test
    model = ChangeTaskModel.load_from_checkpoint(gdf=gdf, seg_conf=seg_conf, checkpoint_path=checkpoint)

    tb_logger = pl_loggers.TensorBoardLogger(save_dir=seg_conf.path_model_log)
    pred_logger = LogPredictionsCallback(seg_conf=seg_conf)
    metric_callback = MetricCallBack(seg_conf=seg_conf)
    # arguments made to CometLogger are passed on to the comet_ml.Experiment class

    """
    comet_logger = pl_loggers.CometLogger(
        api_key="ALpCE5ENfXcq3gmsqs2pfVBiD",
        # save_dir=".",  # Optional
        project_name="poc_gers",  # Optional
        experiment_name="test-comet-2",  # Optional
    )
    """

    # wandb_logger = pl_loggers.WandbLogger(seg_conf.project_name,)
    # callbacks = [model_checkpoint]

    trainer = Trainer(
                      gpus=seg_conf.gpu,
                      accelerator=None,
                      progress_bar_refresh_rate=2)

    trainer.test(test_dataloaders=data_module.test_dataloader(),
                 model=model)


if __name__ == "__main__":

    main()
