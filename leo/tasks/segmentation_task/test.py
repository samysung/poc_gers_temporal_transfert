import os
from pathlib import Path
import matplotlib.pyplot as plt
# import comet_ml
import torch
import torchvision
from kornia import tensor_to_image
import datetime
from dataclasses import asdict
from leo.tasks.segmentation_task.transform import DeNormalize
from leo.core.default_conf import EnvConf
from leo.core.constants import IMAGENET
from leo.core.image import tensor_to_image
from leo.tasks.segmentation_task.conf import SegmentationTaskConf, TrainConf, ValConf, TestConf
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from pytorch_lightning import loggers as pl_loggers
from leo.tasks.segmentation_task.data_module import SegmentationTaskDataModule
from leo.tasks.segmentation_task.model import SemanticSegmentationTaskModel
from leo.tasks.segmentation_task.callback import LogPredictionsCallback, MetricCallBack
from leo.core.io_utils import save_dict_as_json


def main():

    checkpoint = "/var/data/dl/gers/log/segmentation/segmentation_mono_temporelle_mono_style_2016_training-sgd-RGB/checkpoint/epoch-epoch=424-loss-mean_val_iou=0.56.ckpt"
    seg_conf = SegmentationTaskConf()
    print(seg_conf)
    save_dict_as_json(asdict(seg_conf), os.path.join(seg_conf.path_model_output, "hparams.json"))
    data_module = SegmentationTaskDataModule(seg_conf=seg_conf)
    data_module.setup(stage="test")
    gdf = data_module.db_test
    test_gdf = data_module.db_test
    model = SemanticSegmentationTaskModel.load_from_checkpoint(gdf=gdf, seg_conf=seg_conf, checkpoint_path=checkpoint,
                                                               test_gdf=test_gdf)
    tb_logger = pl_loggers.TensorBoardLogger(save_dir=seg_conf.path_model_log)
    pred_logger = LogPredictionsCallback(seg_conf=seg_conf)
    metric_callback = MetricCallBack(seg_conf=seg_conf)
    # arguments made to CometLogger are passed on to the comet_ml.Experiment class

    """
    comet_logger = pl_loggers.CometLogger(
        api_key="ALpCE5ENfXcq3gmsqs2pfVBiD",
        # save_dir=".",  # Optional
        project_name="poc_gers",  # Optional
        experiment_name="test-comet-2",  # Optional
    )
    """

    # wandb_logger = pl_loggers.WandbLogger(seg_conf.project_name,)
    # callbacks = [model_checkpoint]

    trainer = Trainer(
                      gpus=seg_conf.gpu,
                      accelerator=None,
                      progress_bar_refresh_rate=2)

    trainer.test(test_dataloaders=data_module.test_dataloader(),
                 model=model)


if __name__ == "__main__":

    main()
