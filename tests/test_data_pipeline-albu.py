""" test data reading, augmentation, normalization """
import os
import matplotlib.pyplot as plt
import geopandas as gpd
from leo.tasks.segmentation_task.data_module import SegmentationTaskDataModule
from leo.tasks.segmentation_task.transform import DeNormalize
from leo.core.constants import IMAGENET

ROOT = "/media/HP-2007S005-data"
data_dir = os.path.join(ROOT, "gers/supervised_dataset")
db_path = "supervised_dataset_with_stats.geojson"
db = gpd.read_file(os.path.join(data_dir, db_path))
batch_size = 2
gers_data = SegmentationTaskDataModule(data_dir=data_dir, db_path=db_path, fold=1, batch_size=batch_size)
gers_data.setup()
apply_hard_radiometrc_transform = False
denorm = DeNormalize(mean=IMAGENET["mean"], std=IMAGENET["std"])


def test():

    fig = plt.figure(figsize=(20, 20))
    print(gers_data.db.columns)
    # exit(0)
    train_loader = gers_data.train_dataloader()
    samples = next(iter(train_loader))
    """
    for sample in samples:

        index, img_aug, mask_aug = sample["index"], sample["img"], sample["mask"]

        print(img.shape)
        print(mask.shape)
        print(img_aug.shape)
        print(mask_aug.shape)
        fig.suptitle(f'example image {idx}', fontsize=16)
        ax = []

        ax.append(fig.add_subplot(2, 2, 1))
        ax[-1].clear()
        ax[-1].set_title("image before aug")
        plt.imshow(img.transpose((1, 2, 0)))
        # axarr[0, 0].set_title("img 1")

        ax.append(fig.add_subplot(2, 2, 2))
        ax[-1].clear()
        ax[-1].set_title("image after aug")
        plt.imshow(img_aug.transpose((1, 2, 0)))

        ax.append(fig.add_subplot(2, 2, 3))
        ax[-1].clear()
        ax[-1].set_title("mask before mask")
        plt.imshow(img.transpose((1, 2, 0)))
        # axarr[0, 0].set_title("img 1")

        ax.append(fig.add_subplot(2, 2, 4))
        ax[-1].clear()
        ax[-1].set_title("mask after mask")
        plt.imshow(img_aug.transpose((1, 2, 0)))
        # axarr[0, 1].set_title("img 2")
        # axarr[1, 1].set_title("sub img 2")
        plt.pause(3)
        """




if __name__ == "__main__":

    test()